/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektAPI.Spigot.Utils;

import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;

import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class ActionBar {
	
	public static void sendActionBar(Player p, String text){
		IChatBaseComponent comp = ChatSerializer.a("{\"text\": \""+text+"\"}");
		PacketPlayOutChat paction = new PacketPlayOutChat(comp, (byte) 2);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(paction);
	}

}
