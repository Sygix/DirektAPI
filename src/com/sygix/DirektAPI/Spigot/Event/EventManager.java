/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektAPI.Spigot.Event;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import com.sygix.DirektAPI.Spigot.Event.Players.PlayerJoin;
import com.sygix.DirektAPI.Spigot.Event.Players.PlayerQuit;

public class EventManager {
	
	public static void registerEvent(Plugin pl){
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new PlayerJoin(), pl);
		pm.registerEvents(new PlayerQuit(), pl);
		pm.registerEvents(new Death(), pl);
	}

}
