/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektAPI.Spigot.Event;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.sygix.DirektAPI.Spigot.mainSpigot;
import com.sygix.DirektAPI.Spigot.Utils.PlayerAutoRespawnEvent;
import com.sygix.DirektAPI.Spigot.Utils.PlayerPreAutoRespawnEvent;

public class Death implements Listener {
	
	@EventHandler
	public void onPlayerDeath(final PlayerDeathEvent e) {

		final Location deathLoc = e.getEntity().getLocation();

		final Player player = e.getEntity();

		PlayerPreAutoRespawnEvent ppare = new PlayerPreAutoRespawnEvent(player, deathLoc);

		Bukkit.getPluginManager().callEvent(ppare);

		if (ppare.isCancelled())return;

		Bukkit.getScheduler().scheduleSyncDelayedTask(mainSpigot.getInstance(), new Runnable() {
			@Override
			public void run() {
				player.spigot().respawn();
				Location respawnLoc = deathLoc;
				Bukkit.getPluginManager().callEvent(new PlayerAutoRespawnEvent(e.getEntity(), deathLoc, respawnLoc));
			}
		}, 1L);
	}

}
