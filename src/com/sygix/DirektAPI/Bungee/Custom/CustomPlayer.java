/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektAPI.Bungee.Custom;

import java.util.ArrayList;
import java.util.Iterator;

import net.md_5.bungee.api.connection.ProxiedPlayer;

public class CustomPlayer {

	private static ArrayList<CustomPlayer> registry = new ArrayList<CustomPlayer>();
	  
	   private ProxiedPlayer player;
	  
	   public CustomPlayer(ProxiedPlayer player){
	     this.player = player;
	   }
	   
	   public void setPlayer(ProxiedPlayer player) {
		     this.player = player;
		   }
		  
	   public static void registerPlayer(CustomPlayer customplayer){
		   registry.add(customplayer);
	   }
		  
	   public static void unregisterPlayer(CustomPlayer customplayer){
		   registry.remove(customplayer);
	   }
		  
	   @SuppressWarnings("rawtypes")
	   public static CustomPlayer getPlayer(ProxiedPlayer player){
		   Iterator iterator = registry.iterator();
		   while(iterator.hasNext()){
			   CustomPlayer customplayer = (CustomPlayer)iterator.next();
			   if(customplayer.getPlayer() == player)return customplayer;
		   }
		   return null;
	   }

	   public ProxiedPlayer getPlayer() {
	     return player;
	   }
}