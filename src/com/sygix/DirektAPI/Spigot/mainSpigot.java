/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektAPI.Spigot;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.Configuration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.sygix.DirektAPI.Spigot.SQL.SQLManager;

public class mainSpigot extends JavaPlugin{
	
	public static mainSpigot instance;
	public static Configuration config;
	
	@Override
	public void onEnable(){
		if(!this.getDataFolder().exists()){
			this.getDataFolder().mkdir();
		}
		this.saveDefaultConfig();
		config = this.getConfig();
		instance = this;
		try {
			SQLManager.connect();
			SQLManager.registerPlugin();
			SQLManager.registerServerOnBDD(Bukkit.getServerName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*** Code ici ***/
		
		/*** Fin du code ***/
		getLogger().info("\n---- "+getDescription().getName()+" "+getDescription().getVersion()+" : Loaded ----\n");
	}
	
	@Override
	public void onDisable(){
		/*** Code ici ***/
		
		/*** Fin du code ***/
		try {
			SQLManager.disconnect();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		getLogger().info("\n---- "+getDescription().getName()+" "+getDescription().getVersion()+" : Unloaded ----\n");
	}

	public static Plugin getInstance() {
		return instance;
	}

}
