/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektAPI.Spigot.Custom;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;


public class Menu {
	
	static HashMap<Player, Menu> menus = new HashMap<Player, Menu>();
	static Inventory inv;
	static Player p;
	
	public Menu (Inventory inv, Player p){
		Menu.inv = inv;
		Menu.p = p;
	}
	
	public static Menu createMenu(String name, Player p, int size){
		inv = Bukkit.createInventory(p, size, name);
		menus.put(p, (Menu) inv);
		return (Menu) inv;
	}
	
	public static Menu getMenu(Player p){
		inv = (Inventory) menus.get(p);
		Menu.p = p;
		return (Menu) inv;
	}
	
	public static void removeMenu(Player p){
		menus.remove(p);
	}
	
	public static void addItem(ItemStack item, int place){
		inv.setItem(place, item);
	}
	
	@Deprecated
	public static void removeItem(int place){
		inv.remove(place);
	}
	
	public static void updateName(String name){
		Inventory inv = Bukkit.createInventory(p, Menu.inv.getSize(), name);
		Menu.inv = inv;
	}
	
	public static ItemStack getItem(int place){
		ItemStack item = inv.getItem(place);
		return item;
	}
	
	public static ItemStack[] getItems(){
		return inv.getContents();
	}
	
	public static void setItems(ItemStack[] items){
		if(items.length <= 54 && items.length >= 0){
			inv.setContents(items);
		}
	}

}
