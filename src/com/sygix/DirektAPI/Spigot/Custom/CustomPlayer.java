/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektAPI.Spigot.Custom;

import java.util.ArrayList;
import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class CustomPlayer {

	private static ArrayList<CustomPlayer> registry = new ArrayList<CustomPlayer>();
	  
	   private Player player;
	  
	   public CustomPlayer(Player player){
	     this.player = player;
	   }
	   
	   public void setPlayer(Player player) {
		     this.player = player;
		   }
		  
	   public static void registerPlayer(CustomPlayer customplayer){
		   registry.add(customplayer);
	   }
		  
	   public static void unregisterPlayer(CustomPlayer customplayer){
		   registry.remove(customplayer);
	   }
		  
	   @SuppressWarnings("rawtypes")
	   public static CustomPlayer getPlayer(Player player){
		   Iterator iterator = registry.iterator();
		   while(iterator.hasNext()){
			   CustomPlayer customplayer = (CustomPlayer)iterator.next();
			   if(customplayer.getPlayer() == player)return customplayer;
		   }
		   return null;
	   }

	   public Player getPlayer() {
	     return player;
	   }
	   
	   public Location getLocation(){
		   return this.player.getLocation();
	   }
	   
	   public String getServer(){
		   return Bukkit.getServerName();
	   }
}