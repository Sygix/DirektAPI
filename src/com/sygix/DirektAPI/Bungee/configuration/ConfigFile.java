/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektAPI.Bungee.configuration;

import java.io.File;
import java.io.IOException;

import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import com.sygix.DirektAPI.Bungee.mainBungee;

public class ConfigFile {
	
	public static Configuration config;
	
	public static void saveDefaultConfig(){
		try{
			File file = new File(mainBungee.instance.getDataFolder().getPath(), "config.yml");
			if(!file.exists()){
				file.createNewFile();
				config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
				config.set("SQL.User", "root");
				config.set("SQL.Pass", "****");
				config.set("SQL.URL", "jdbc:mysql://mc.direktserv.fr/Bungee");
				ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, file);
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static Configuration getConfig(){
		return config;
	}
	
	public static void saveConfig(){
		File file = new File(mainBungee.instance.getDataFolder().getPath(), "config.yml");
		try {
			ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
