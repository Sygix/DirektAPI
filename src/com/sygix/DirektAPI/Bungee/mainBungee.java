/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektAPI.Bungee;

import java.sql.SQLException;

import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;

import com.sygix.DirektAPI.Bungee.SQL.SQLManager;
import com.sygix.DirektAPI.Bungee.configuration.ConfigFile;

public class mainBungee extends Plugin{
	
	public static mainBungee instance;
	public static Configuration config;
	
	@Override
	public void onEnable(){
		if(!this.getDataFolder().exists()){
			this.getDataFolder().mkdir();
		}
		ConfigFile.saveDefaultConfig();
		instance = this;
		try {
			SQLManager.connect();
			SQLManager.registerPlugin();
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*** Code ici ***/
		
		/*** Fin du code ***/
		getLogger().info("\n---- "+getDescription().getName()+" "+getDescription().getVersion()+" : Loaded ----\n");
	}
	
	@Override
	public void onDisable(){
		/*** Code ici ***/
		
		/*** Fin du code ***/
		ConfigFile.saveConfig();
		try {
			SQLManager.disconnect();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		getLogger().info("\n---- "+getDescription().getName()+" "+getDescription().getVersion()+" : Unloaded ----\n");
	}

}
