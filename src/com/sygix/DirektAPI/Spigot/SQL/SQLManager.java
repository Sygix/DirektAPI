/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektAPI.Spigot.SQL;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.entity.Player;

import com.sygix.DirektAPI.Spigot.mainSpigot;

public class SQLManager {
	
	public static Connection con = null;

    public static void connect() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        String url_ = mainSpigot.config.get("SQL.URL").toString();
        String pass = mainSpigot.config.get("SQL.Pass").toString();
        String user = mainSpigot.config.get("SQL.User").toString();

        con = DriverManager.getConnection(url_, user, pass);
    }
    
    public static void disconnect() throws SQLException {
    	if(con != null){
    		con.close();
    	}
    }
    
    @SuppressWarnings("unused")
    public static void registerPlugin() throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        try {
            ResultSet res = state.executeQuery("SELECT * FROM players");
        } catch(SQLException e) {
            state.execute("CREATE TABLE players ( id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, uid VARCHAR(100), pseudo VARCHAR(100), time LONG, lastconnect LONG, coins DOUBLE, halo DOUBLE, date_buy_halo VARCHAR(100), parrain VARCHAR(100), optionshide BOOLEAN)");
        }
        try{
        	ResultSet res = state.executeQuery("SELECT * FROM servers");
        }catch(SQLException e){
        	state.execute("CREATE TABLE servers ( id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, serveur VARCHAR(100), state VARCHAR(100), joueurs INT, maintenance BOOLEAN)");
        }
        state.close();
    }
    
    public static boolean registerOnBDD(Player p) throws SQLException, IOException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='" + p.getUniqueId()+ "'");
        while(!res.next()){
        	Statement s = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            s.executeUpdate("INSERT INTO players (id, uid, pseudo, time, lastconnect, coins, halo, date_buy_halo, optionshide) VALUES (null, '"+p.getUniqueId()+"', '"+p.getName()+"', 0, 0, 0.0, 1.0, null, 0)");
            state.close();
            return false;
        }
        state.close();
        return true;
    }
    
    public static void setCoins(Player p, double coins) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE players SET coins="+coins+" WHERE uid='"+p.getUniqueId()+"'");
        state.close();
    }
    
    public static double getCoins(Player p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
            double coins = res.getDouble("coins");
            state.close();
            return coins;
        }
        state.close();
        return 0.0;
    }
    
    public static double getCoins(String p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE pseudo='"+p+"'");
        while(res.next()) {
            double coins = res.getDouble("coins");
            state.close();
            return coins;
        }
        state.close();
        return 0.0;
    }
    
    public static long getTime(Player p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
            long time = res.getLong("time");
            state.close();
            return time;
        }
        state.close();
        return 0;
    }
    
    public static long getTime(String p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE pseudo='"+p+"'");
        while(res.next()) {
            long time = res.getLong("time");
            state.close();
            return time;
        }
        state.close();
        return 0;
    }
    
    public static void setHalo(Player p, double halo) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE players SET halo="+halo+" WHERE uid='"+p.getUniqueId()+"'");
        state.close();
    }
    
    public static double getHalo(Player p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
            double halo = res.getDouble("halo");
            state.close();
            return halo;
        }
        state.close();
        return 0.0;
    }
    
    public static double getHalo(String p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE pseudo='"+p+"'");
        while(res.next()) {
            double halo = res.getDouble("halo");
            state.close();
            return halo;
        }
        state.close();
        return 0.0;
    }
    
    public static void setDateBuyHalo(Player p, String date) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE players SET date_buy_halo="+date+" WHERE uid='"+p.getUniqueId()+"'");
        state.close();
    }
    
    public static String getDateBuyHalo(Player p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
        	String date = res.getString("date_buy_halo");
            state.close();
            return date;
        }
        state.close();
        return null;
    }
    
    public static boolean getOptionsHide(Player p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
            boolean option = res.getBoolean("optionshide");
            state.close();
            return option;
        }
        state.close();
        return false;
    }
    
    public static void setOptionsHide(Player p, boolean option) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE players SET optionshide="+option+" WHERE uid='"+p.getUniqueId()+"'");
        state.close();
    }
    
//serveurs
    
    public static boolean registerServerOnBDD(String server) throws SQLException, IOException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM servers WHERE serveur='" + server + "'");
        while(!res.next()){
        	Statement s = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            s.executeUpdate("INSERT INTO servers (id, serveur, state, joueurs, maintenance) VALUES (null, '"+server+"', null, 0, 0)");
            state.close();
            return false;
        }
        state.close();
        return true;
    }
    
    public static void setState(String serveur, String State) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE servers SET state='"+State+"' WHERE serveur='"+serveur+"'");
        state.close();
    }
    
    public static String getState(String serveur) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM servers WHERE serveur='"+serveur+"'");
        while(res.next()) {
            String State = res.getString("state");
            state.close();
            return State;
        }
        state.close();
        return null;
    }
    
    public static void setJoueurs(String serveur, int nbr) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE servers SET joueurs="+nbr+" WHERE serveur='"+serveur+"'");
        state.close();
    }
    
    public static int getJoueurs(String serveur) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM servers WHERE serveur='"+serveur+"'");
        while(res.next()) {
            int nbr = res.getInt("joueurs");
            state.close();
            return nbr;
        }
        state.close();
        return 1812;
    }

}
