/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.DirektAPI.Spigot.Utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class BroadcastMessage {
	
	public static void broadcast(String M){
		for(Player pls : Bukkit.getServer().getOnlinePlayers()){
			pls.sendMessage(M);
		}
	}

}
